Preferences.addAll([
  {
    id: "extensions.allowhtmltemp.ButtonFunction",
    type: "int"
  },
  {
    id: "mailnews.message_display.disable_remote_image",
    type: "bool",
    inverted: true
  },
  {
    id: "extensions.allowhtmltemp.ForceRemoteContent",
    type: "bool"
  },
  {
    id: "javascript.enabled",
    type: "bool"
  },
  {
    id: "extensions.allowhtmltemp.JavaScriptTemp",
    type: "bool"
  },
  {
    id: "mail.inline_attachments",
    type: "bool"
  },
  {
    id: "extensions.allowhtmltemp.InlineAttachmentsTemp",
    type: "bool"
  },
]);