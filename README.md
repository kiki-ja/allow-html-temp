***Allow HTML Temp***

--------

### Features

* Allows to have HTML temporarily allowed in the currently displayed message by only one click. When switching to another message, it'll be shown automatically again in plain text or simple html mode (if this is your default mode).

### Known issues

* 

### Installation

1. [Download Allow HTML Temp from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/allow-html-temp/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/en-US/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations (i.e. translations) of Quote Colors via [Crowdin](https://crowdin.com/project/allow-html-temp) (if your language is not listed, please create an [issue](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/issues/))
* creating [issues](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/issues/) about possible improvements


### Coders

* Alexander Ihrig (Original Author and Maintainer)

### Translators

* 


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/LICENSE)